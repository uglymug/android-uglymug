package mobi.uglymug.ui.tutorial;

import android.content.Context;
import android.content.SharedPreferences;

public class TutorialPreferences {

    private SharedPreferences mSharedPreferences;

    public TutorialPreferences(Context context) {
        mSharedPreferences = context.getSharedPreferences(TutorialPreferences.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    public boolean hasShown(Tutorial tutorial) {
        return mSharedPreferences.getBoolean(tutorial.name(), false);
    }

    public void setShown(Tutorial tutorial, boolean shown) {
        mSharedPreferences.edit().putBoolean(tutorial.name(), shown).apply();
    }

    public static enum Tutorial {
        EXPOSURE,
        RATING,
        SHARE
    }
}
