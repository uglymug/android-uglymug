package mobi.uglymug.ui.camera;

import android.graphics.Bitmap;

public class CameraEvent {

    private CameraEvent() {}

    public static class PreviewStartedEvent {}
    public static class ActionSnapEvent {}

    public static class SnapTakenEvent {
        private Bitmap mBitmap;

        public SnapTakenEvent(Bitmap bitmap) {
            mBitmap = bitmap;
        }

        public Bitmap getBitmap() {
            return mBitmap;
        }
    }
}
