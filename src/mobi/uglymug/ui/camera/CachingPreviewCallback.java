package mobi.uglymug.ui.camera;

import java.util.Arrays;

import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;

class CachingPreviewCallback implements PreviewCallback {

    private byte[] mData;

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        mData = data;
    }

    public byte[] getData() {
        return Arrays.copyOf(mData, mData.length);
    }
}