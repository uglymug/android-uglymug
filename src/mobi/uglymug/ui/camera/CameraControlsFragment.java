package mobi.uglymug.ui.camera;

import mobi.uglymug.R;
import mobi.uglymug.ui.camera.CameraEvent.ActionSnapEvent;
import mobi.uglymug.ui.camera.CameraEvent.PreviewStartedEvent;
import mobi.uglymug.view.text.animation.AnimationTextView;
import mobi.uglymug.view.text.animation.TypeWriterTextAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.halfbit.tinybus.Bus;
import com.halfbit.tinybus.Subscribe;
import com.halfbit.tinybus.TinyBus;

public class CameraControlsFragment extends Fragment {

    private static final String TAG = CameraControlsFragment.class.getSimpleName();

    private View mTakePhotoView;
    private Bus mBus;

    private AnimationTextView mSpeechBubble;

    public static final CameraControlsFragment newInstance() {
        return new CameraControlsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBus = TinyBus.from(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_camera_controls, container, false);

        mTakePhotoView = view.findViewById(R.id.take_photo);
        mTakePhotoView.setOnClickListener(mOnTakePhotoClicked);

        mSpeechBubble = (AnimationTextView) view.findViewById(R.id.bubble_speech);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Subscribe
    public void onPreviewStarted(PreviewStartedEvent event) {
        show();
    }

    public void show() {
        getFragmentManager().beginTransaction().show(this).commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
        mSpeechBubble.animateText(new TypeWriterTextAnimator(getString(R.string.pug_mug_motivation), 30));
    }

    public void hide() {
        getFragmentManager().beginTransaction().hide(this).commitAllowingStateLoss();
        mSpeechBubble.setText(null);
    }

    private OnClickListener mOnTakePhotoClicked = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mBus.post(new ActionSnapEvent());
            hide();
        }
    };
}
