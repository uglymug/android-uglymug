package mobi.uglymug.ui.camera;

import java.io.ByteArrayOutputStream;
import java.util.List;

import mobi.uglymug.BuildConfig;
import mobi.uglymug.R;
import mobi.uglymug.ui.camera.CameraEvent.ActionSnapEvent;
import mobi.uglymug.ui.pug.PugEvent.ActionScreenshotEvent;
import mobi.uglymug.ui.pug.PugFragment;
import mobi.uglymug.ui.tutorial.TutorialPreferences;
import mobi.uglymug.ui.tutorial.TutorialPreferences.Tutorial;
import mobi.uglymug.util.Bitmaps;
import mobi.uglymug.util.Cameras;
import mobi.uglymug.util.Cameras.CameraType;
import mobi.uglymug.util.Screenshots;
import mobi.uglymug.util.Screenshots.OnScreenshotTaken;
import mobi.uglymug.view.camera.CameraPreview;
import mobi.uglymug.view.camera.CameraPreview.PreviewReadyCallback;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.halfbit.tinybus.Bus;
import com.halfbit.tinybus.Subscribe;
import com.halfbit.tinybus.TinyBus;

public class CameraActivity extends Activity {

    private static final String FRAG_PREVIEW_OVERLAY = "FRAG_PREVIEW_OVERLAY";
    private static final String FRAG_PUG_OVERLAY = "FRAG_PUG_OVERLAY";
    private static final String TAG = CameraActivity.class.getSimpleName();

    private CameraPreview mPreview;
    private RelativeLayout mRoot;
    private Camera mCamera;
    private Bus mBus;

    private int mMinExposureCompensation;
    private int mMaxExposureCompensation;
    private int mCurrentExposireCompensation;

    private Display mDisplay;
    private ImageView mSnapshotImageView;
    private UiState mUiState;
    private GestureDetectorCompat mGestureDetector;
    private View mExposureTutorialView;
    private TutorialPreferences mTutorialPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mBus = TinyBus.from(this);
        mDisplay = getWindowManager().getDefaultDisplay();
        mTutorialPreferences = new TutorialPreferences(this);

        mRoot = (RelativeLayout) findViewById(R.id.root);
        mSnapshotImageView = (ImageView) findViewById(R.id.img_snapshot);
        mGestureDetector = new GestureDetectorCompat(this, mOnGestureListener);

        if (BuildConfig.DEBUG) {
            Log.v(TAG, "    MANUFACTURER:" + Build.MANUFACTURER);
            Log.v(TAG, "    PRODUCT:" + Build.PRODUCT);
            Log.v(TAG, "    MODEL:" + Build.MODEL);
            Log.v(TAG, "    TYPE:" + Build.TYPE);
        }

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            CameraControlsFragment controlsFragment = CameraControlsFragment.newInstance();
            transaction.add(R.id.root, controlsFragment, FRAG_PREVIEW_OVERLAY);
            transaction.hide(controlsFragment);

            PugFragment pugFragment = PugFragment.newInstance();
            transaction.add(R.id.root, pugFragment, FRAG_PUG_OVERLAY);
            transaction.hide(pugFragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        Log.v(TAG, "[onBackPressed] UiState=" + mUiState);
        if (mUiState == UiState.SNAPSHOT) {
            setUiState(UiState.PREVIEW);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPreview = new CameraPreview(this, getWindowManager().getDefaultDisplay(),
                Cameras.getCameraIdForType(CameraType.FRONT), CameraPreview.LayoutMode.FitToParent);
        mPreview.setOnPreviewReady(mPreviewReadyCallback);
        mRoot.addView(mPreview, 0, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        showTutorial();

        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mPreview.setPreviewCallback(null);
        mPreview.stop();
        mRoot.removeView(mPreview); // This is necessary.
        mPreview = null;
        mBus.unregister(this);
    }

    @Subscribe
    public void onTakePhotoEvent(ActionSnapEvent event) {
        hideTutorial();
        takePhoto();
    }

    @Subscribe
    public void onTakeScreenshot(ActionScreenshotEvent event) {
        Log.v(TAG, "[onTakeScreenshot]");
        // TODO: Need to make the screenshot happen in the background
        // Toast.makeText(this, R.string.taking_screenshot, Toast.LENGTH_LONG).show();
        Screenshots.takeScreenshot(mRoot, mOnScreenshotTaken);
    }

    private void setUiState(UiState state) {
        switch (state) {
            case PREVIEW:
                mSnapshotImageView.setVisibility(View.GONE);
                mBus.post(new CameraEvent.PreviewStartedEvent());
                break;
            case SNAPSHOT:
                mSnapshotImageView.setVisibility(View.VISIBLE);
                break;
        }
        mUiState = state;
    }

    private static enum UiState {
        PREVIEW,
        SNAPSHOT
    }

    private void showTutorial() {
        if (!mTutorialPreferences.hasShown(Tutorial.EXPOSURE)) {
            mExposureTutorialView = LayoutInflater.from(this).inflate(R.layout.tutorial_camera_controls, mRoot, false);
            mExposureTutorialView.setAlpha(0f);
            mRoot.addView(mExposureTutorialView);
            ObjectAnimator.ofFloat(mExposureTutorialView, View.ALPHA, 1f).start();
        }
    }

    private void hideTutorial() {
        if (mExposureTutorialView != null) {
            ObjectAnimator.ofFloat(mExposureTutorialView, View.ALPHA, 0f).start();
        }

        mTutorialPreferences.setShown(Tutorial.EXPOSURE, true);
    }

    private void takePhoto() {
        if (mCamera == null) {
            Log.w(TAG, "[takePhoto] Camera not available!");
            return;
        }

        byte[] data = mCachingPreviewCallback.getData();
        if (data == null) {
            Log.w(TAG, "Didn't get a preview frame!");
            return;
        }

        Camera.Parameters parameters = mCamera.getParameters();
        int format = parameters.getPreviewFormat();
        if (format == ImageFormat.NV21 || format == ImageFormat.YUY2 || format == ImageFormat.NV16) {
            int w = parameters.getPreviewSize().width;
            int h = parameters.getPreviewSize().height;
            // Get the YuV image
            YuvImage yuv_image = new YuvImage(data, format, w, h, null);
            // Convert YuV to Jpeg
            Rect rect = new Rect(0, 0, w, h);
            ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
            yuv_image.compressToJpeg(rect, 100, output_stream);
            data = output_stream.toByteArray();
        }

        Bitmap bitmap = Bitmaps.toBitmap(data);
        bitmap = Bitmaps.rotate(bitmap, Cameras.getRotationAngleForPicture(mDisplay), true);
        mSnapshotImageView.setImageBitmap(bitmap);
        setUiState(UiState.SNAPSHOT);
        mBus.post(new CameraEvent.SnapTakenEvent(bitmap));
    }

    private void decreaseExposure() {
        if (mCamera == null) {
            Log.w(TAG, "[decreaseExposure] Camera not ready!");
            return;
        }

        if (mMinExposureCompensation == 0 && mMaxExposureCompensation == 0) {
            Log.w(TAG, "[decreaseExposure] ExposureCompensation not supported!");
            return;
        }

        if (mCurrentExposireCompensation <= mMinExposureCompensation) {
            Log.v(TAG, "[decreaseExposure] Already at min...");
            return;
        }

        try {
            Parameters parameters = mCamera.getParameters();
            mCurrentExposireCompensation--;
            parameters.setExposureCompensation(mCurrentExposireCompensation);
            mCamera.setParameters(parameters);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void increaseExposure() {
        if (mCamera == null) {
            Log.w(TAG, "[increaseExposure] Camera not ready!");
            return;
        }

        if (mMinExposureCompensation == 0 && mMaxExposureCompensation == 0) {
            Log.w(TAG, "[increaseExposure] ExposureCompensation not supported!");
            return;
        }

        if (mCurrentExposireCompensation >= mMaxExposureCompensation) {
            Log.v(TAG, "[increaseExposure] Already at max...");
            return;
        }

        try {
            Parameters parameters = mCamera.getParameters();
            mCurrentExposireCompensation++;
            parameters.setExposureCompensation(mCurrentExposireCompensation);
            mCamera.setParameters(parameters);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }

    private void setupCamera(Camera camera) {
        Parameters parameters = camera.getParameters();
        List<String> supportedWhiteBalance = parameters.getSupportedWhiteBalance();
        Log.v(TAG, "[setupCamera] supportedWhiteBalance=" + supportedWhiteBalance);
        List<String> supportedSceneModes = parameters.getSupportedSceneModes();
        Log.v(TAG, "[setupCamera] supportedSceneModes=" + supportedSceneModes);

        mMinExposureCompensation = parameters.getMinExposureCompensation();
        mMaxExposureCompensation = parameters.getMaxExposureCompensation();

        camera.setParameters(parameters);
    }

    /**
     * Callback for when preview is ready
     */
    private PreviewReadyCallback mPreviewReadyCallback = new PreviewReadyCallback() {
        @Override
        public void onPreviewReady(Camera camera) {
            Log.v(TAG, "[onPreviewReady]");
            mPreview.setOneShotPreviewCallback(mOneTimePreviewCallback);
        }
    };


    /**
     * Callback for when a preview frame occurs.
     */
    private PreviewCallback mOneTimePreviewCallback = new PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            Log.v(TAG, "[onPreviewFrame] camera=" + camera);
            mCamera = camera;
            mPreview.setPreviewCallback(mCachingPreviewCallback);
            setUiState(UiState.PREVIEW);

            setupCamera(camera);
        }
    };

    private CachingPreviewCallback mCachingPreviewCallback = new CachingPreviewCallback();

    private OnScreenshotTaken mOnScreenshotTaken = new OnScreenshotTaken() {

        @Override
        public void onScreenshotTaken(Bitmap bitmap) {
            String pathofBmp = Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
            if (pathofBmp != null) {
                Uri bmpUri = Uri.parse(pathofBmp);
                final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.setType("image/png");
                startActivity(shareIntent);
            } else {
                Toast.makeText(CameraActivity.this, R.string.error_taking_screenshot, Toast.LENGTH_LONG).show();
            }
        }
    };

    private OnGestureListener mOnGestureListener = new OnGestureListener() {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {}

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (distanceY > 10f) {
                increaseExposure();
            } else if (distanceY < -10f) {
                decreaseExposure();
            }
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {}

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            hideTutorial();
            return false;
        }
    };
}
