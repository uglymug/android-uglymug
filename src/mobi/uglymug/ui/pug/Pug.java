package mobi.uglymug.ui.pug;

import mobi.uglymug.ui.pug.expressions.ExpressionGenerator;
import mobi.uglymug.ui.pug.expressions.PugExpression;
import mobi.uglymug.ui.pug.expressions.PugExpression.PugRating;

public class Pug {

    private String mName;
    private int mDefaultResource;
    private ExpressionGenerator mInsultGenerator;

    public Pug(String name, int defaultResource, ExpressionGenerator insultGenerator) {
        mName = name;
        mDefaultResource = defaultResource;
        mInsultGenerator = insultGenerator;
    }

    public String getName() {
        return mName;
    }

    public int getDefaultResource() {
        return mDefaultResource;
    }

    public PugExpression getErrorExtression() {
        return mInsultGenerator.generateErrorExpression();
    }

    public PugExpression getExpression(PugRating rating) {
        return mInsultGenerator.generateExpression(rating);
    }
}
