package mobi.uglymug.ui.pug.expressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mobi.uglymug.ui.pug.expressions.PugExpression.PugRating;
import mobi.uglymug.ui.pug.expressions.PugExpression.PugRatingRange;

public class RangeExpressionGenerator implements ExpressionGenerator {
    private Map<PugRatingRange, PugExpression> mExpressions = new IdentityHashMap<PugRatingRange, PugExpression>();
    private int mErrorDrawable;
    private String mErrorPhrase;

    public RangeExpressionGenerator(int errorDrawable, String errorPhrase) {
        mErrorDrawable = errorDrawable;
        mErrorPhrase = errorPhrase;
    }

    public void addExpression(PugRatingRange range, PugExpression expression) {
        mExpressions.put(range, expression);
    }

    @Override
    public PugExpression generateErrorExpression() {
        return new PugExpression(mErrorDrawable, mErrorPhrase);
    }

    @Override
    public PugExpression generateExpression(PugRating rating) {
        List<PugExpression> availableExpressions = new ArrayList<PugExpression>();
        for (Entry<PugRatingRange, PugExpression> entry : mExpressions.entrySet()) {
            final PugRatingRange range = entry.getKey();
            if (rating.getValue() >= range.getMin().getValue() || rating.getValue() <= range.getMax().getValue()) {
                availableExpressions.add(entry.getValue());
            }
        }

        if (availableExpressions.size() == 0) {
            return generateErrorExpression();
        }

        Collections.shuffle(availableExpressions);
        return availableExpressions.get(0);
    }
}
