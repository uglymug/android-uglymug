package mobi.uglymug.ui.pug.expressions;

public class PugExpression {

    private int mResource;
    private String mPhrase;

    public PugExpression(int resource, String phrase) {
        mResource = resource;
        mPhrase = phrase;
    }

    public int getResource() {
        return mResource;
    }

    public String getPhrase() {
        return mPhrase;
    }

    public static class PugRating {
        public static final int MIN = 0;
        public static final int MAX = 1000;

        private int mValue;

        public PugRating(int value) {
            if (value < MIN || value > MAX) {
                throw new IllegalArgumentException();
            }

            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    public static class PugRatingRange {
        private PugRating mMin;
        private PugRating mMax;

        /**
         *
         * @param min inclusive
         * @param max inclusive
         */
        public PugRatingRange(PugRating min, PugRating max) {
            mMin = min;
            mMax = max;
        }

        public PugRating getMin() {
            return mMin;
        }

        public PugRating getMax() {
            return mMax;
        }
    }
}
