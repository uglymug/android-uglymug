package mobi.uglymug.ui.pug.expressions;

import mobi.uglymug.ui.pug.expressions.PugExpression.PugRating;

public interface ExpressionGenerator {
    public PugExpression generateErrorExpression();
    public PugExpression generateExpression(PugRating rating);
}
