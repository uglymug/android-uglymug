package mobi.uglymug.ui.pug.expressions;

import java.util.Random;

import mobi.uglymug.ui.pug.expressions.PugExpression.PugRating;



public class RandomExpressionGenerator implements ExpressionGenerator {

    private static final int MAX_ADJ_IN_INSULT = 3;
    private Random mRandom = new Random();
    private int[] mDrawablesRes;
    private String[] mClauses;
    private String[] mAdjs;
    private String[] mNouns;
    private String[] mErrors;

    public RandomExpressionGenerator(int[] drawablesRes, String[] errors, String[] clauses, String[] adjs, String[] nouns) {
        mDrawablesRes = drawablesRes;
        mErrors = errors;
        mClauses = clauses;
        mAdjs = adjs;
        mNouns = nouns;
    }

    public int getRandomDrawable() {
        return mDrawablesRes[mRandom.nextInt(mDrawablesRes.length)];
    }

    @Override
    public PugExpression generateErrorExpression() {
        return new PugExpression(getRandomDrawable(), mErrors[mRandom.nextInt(mErrors.length)]);
    }

    @Override
    public PugExpression generateExpression(PugRating rating) {

        String insult;               // The insult
        int index;                   // index of adjective to be added
        int numAdj = mRandom.nextInt(MAX_ADJ_IN_INSULT); // number of adjectives to add

        boolean[] usedIndices = new boolean[mAdjs.length]; // Which adj were used
        for(int i = 0; i < usedIndices.length; i++) {
            usedIndices[i] = false;
        }

        insult = mClauses[mRandom.nextInt(mClauses.length)];    // Pick a clause
        insult += ", you ";
        for(int i = 0; i < numAdj; i++) {
            if(i > 0) {
                insult += ", ";            // Separate each item with a comma
            }
            do {
                index = mRandom.nextInt(mAdjs.length);  // Pick an adjective

            } while(usedIndices[index]);   // If used previously, try again
            insult += mAdjs[index];          // Use the adjective
            usedIndices[index] = true;     // Mark adjective as used
        }
        if(numAdj > 0) {
            insult += ' ';
        }
        insult += mNouns[mRandom.nextInt(mNouns.length)] + '!';   // Add the noun

        return new PugExpression(mDrawablesRes[mRandom.nextInt(mDrawablesRes.length)], insult);
    }

}
