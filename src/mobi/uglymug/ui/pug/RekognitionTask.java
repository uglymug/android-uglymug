package mobi.uglymug.ui.pug;

import mobi.uglymug.api.rekognition.RekoApi;
import mobi.uglymug.api.rekognition.RekoApi.Request;
import mobi.uglymug.api.rekognition.RekoApi.Response;
import mobi.uglymug.ui.pug.PugFragment.OnTaskCompleteListener;
import mobi.uglymug.util.Bitmaps;

import org.apache.http.message.BasicNameValuePair;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;

public class RekognitionTask extends AsyncTask<Bitmap, Void, Response> {

    private OnTaskCompleteListener<Response> mCompleteListener;
    private RekoApi mRekoApi;

    public RekognitionTask(RekoApi rekoApi, OnTaskCompleteListener<Response> completeListener) {
        mRekoApi = rekoApi;
        mCompleteListener = completeListener;
    }

    @Override
    protected Response doInBackground(Bitmap... params) {
        Request request = mRekoApi.newRequest();
        request.add(new BasicNameValuePair("jobs", "face_aggressive_beauty"));
        request.addImage(Bitmaps.toData(params[0], CompressFormat.JPEG, 80));
        return mRekoApi.execute(request);
    }

    @Override
    protected void onPostExecute(Response result) {
        super.onPostExecute(result);
        mCompleteListener.onComplete(result);
    }
}