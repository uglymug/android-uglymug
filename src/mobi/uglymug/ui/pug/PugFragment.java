package mobi.uglymug.ui.pug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mobi.uglymug.R;
import mobi.uglymug.animation.SimpleAnimatorListener;
import mobi.uglymug.api.rekognition.RekoApi;
import mobi.uglymug.api.rekognition.RekoApi.Face;
import mobi.uglymug.api.rekognition.RekoApi.FaceDetectionResponse;
import mobi.uglymug.api.rekognition.RekoApi.Response;
import mobi.uglymug.properties.PropertiesFileLoader;
import mobi.uglymug.properties.RekognitionProperties;
import mobi.uglymug.ui.camera.CameraEvent;
import mobi.uglymug.ui.camera.CameraEvent.SnapTakenEvent;
import mobi.uglymug.ui.pug.expressions.PugExpression;
import mobi.uglymug.ui.pug.expressions.PugExpression.PugRating;
import mobi.uglymug.ui.pug.expressions.RandomExpressionGenerator;
import mobi.uglymug.ui.tutorial.TutorialPreferences;
import mobi.uglymug.ui.tutorial.TutorialPreferences.Tutorial;
import mobi.uglymug.util.Resources;
import mobi.uglymug.view.text.animation.AnimationTextView;
import mobi.uglymug.view.text.animation.CounterTextAnimator;
import mobi.uglymug.view.text.animation.CounterTextAnimator.AnimationCallbacks;
import mobi.uglymug.view.text.animation.TextAnimator;
import mobi.uglymug.view.text.animation.TypeWriterTextAnimator;

import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.halfbit.tinybus.Bus;
import com.halfbit.tinybus.Subscribe;
import com.halfbit.tinybus.TinyBus;

public class PugFragment extends Fragment {

    private static final String TAG = PugFragment.class.getSimpleName();
    private static final int RATING_CHAR_DELAY_MS = 1;
    private static final int PHRASE_CHAR_DELAY_MS = 30;
    private List<Pug> mPugs = new ArrayList<Pug>();
    private Bus mBus;
    private RekognitionProperties mRekognitionProperties;
    private RekoApi mRekoApi;
    private AsyncTask<Bitmap, Void, Response> mRekoTask;
    private AnimationTextView mRatingTextView;
    private AnimationTextView mPugTextView;
    private ImageView mPugImageView;
    private View mRatingTutorialView;
    private View mShaingTutorialView;
    private View mShareOverlayView;
    private TextView mShareTextView;
    private GestureDetectorCompat mRootGestureDetector;
    private View mRoot;
    private TutorialPreferences mTutorialPreferences;

    public static PugFragment newInstance() {
        return new PugFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBus = TinyBus.from(getActivity());
        mTutorialPreferences = new TutorialPreferences(getActivity());
        try {
            mRekognitionProperties = new RekognitionProperties(new PropertiesFileLoader());
            mRekoApi = new RekoApi(mRekognitionProperties.getApiKey(), mRekognitionProperties.getApiSecret());
        } catch (IOException exception) {
            Toast.makeText(getActivity(), "There was a problem getting the Rekognition settings...", Toast.LENGTH_LONG).show();
        }

        Pug mug = new Pug(getString(R.string.pug_mug_name), R.drawable.pug_mug_smirk,
                new RandomExpressionGenerator(
                        Resources.getResouceIds(getActivity(), R.array.pug_mug_drawables),
                        Resources.getStrings(getActivity(), R.array.pug_mug_errors),
                        Resources.getStrings(getActivity(), R.array.pug_mug_clauses),
                        Resources.getStrings(getActivity(), R.array.pug_mug_adjs),
                        Resources.getStrings(getActivity(), R.array.pug_mug_nouns)));

        mRootGestureDetector = new GestureDetectorCompat(getActivity(), mRootOnGestureListener);

        mPugs.add(mug);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.frag_pug, container, false);
        mRoot.setOnTouchListener(mRootOnTouchListener);

        mRatingTutorialView = mRoot.findViewById(R.id.inc_tutorial_rating);
        mRatingTutorialView.setOnTouchListener(mDismissRatingTutorialOnTouchListener);
        mRatingTutorialView.setVisibility(View.GONE);

        mShaingTutorialView = mRoot.findViewById(R.id.inc_tutorial_share);
        mShaingTutorialView.setOnTouchListener(mDismissSharingTutorialOnTouchListener);
        mShaingTutorialView.setVisibility(View.GONE);

        mShareOverlayView = mRoot.findViewById(R.id.overlay_share);
        mShareOverlayView.setVisibility(View.GONE);
        mShareTextView = (TextView) mShareOverlayView.findViewById(R.id.txt_share);
        mShareTextView.setVisibility(View.GONE);
        mShareTextView.setOnClickListener(mShareTextOnClickListener);

        mRatingTextView = (AnimationTextView) mRoot.findViewById(R.id.pug_rating);
        mPugTextView = (AnimationTextView) mRoot.findViewById(R.id.pug_text);
        mPugImageView = (ImageView) mRoot.findViewById(R.id.pug);

        return mRoot;
    }

    @Override
    public void onResume() {
        super.onResume();

        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }


    @Subscribe
    public void onPreviewStarted(CameraEvent.PreviewStartedEvent event) {
        hide();
    }

    @Subscribe
    public void onSnapTaken(SnapTakenEvent event) {
        show();
        mPugTextView.post(mPugTextLoadingRunnable);
        mPugImageView.setImageResource(getSelectedPug().getDefaultResource());
        makeRekognitionRequest(event.getBitmap(), mOnRekoTaskCompleteListener);
    }

    private Pug getSelectedPug() {
        return mPugs.get(0);
    }

    private void setExpression(PugExpression expression) {
        mPugImageView.setImageResource(expression.getResource());
        mPugTextView.animateText(new TypeWriterTextAnimator(expression.getPhrase(), PHRASE_CHAR_DELAY_MS));
    }

    private void setRating(PugRating pugRating) {
        mRatingTextView.setVisibility(View.VISIBLE);
        mRatingTextView.animateText(new CounterTextAnimator(0, pugRating.getValue(), RATING_CHAR_DELAY_MS, 12, mRatingAnimationCallbacks));
    }

    public void show() {
        getFragmentManager().beginTransaction().show(this).commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions(); // make sure it's executed right away
        mRatingTextView.setVisibility(View.GONE);
    }

    public void hide() {
        getFragmentManager().beginTransaction().hide(this).commitAllowingStateLoss();
        mRatingTextView.setText(null);
        mPugTextView.setText(null);
        cancelLoading();

        hideRatingTutorial(false);
        hideSharingTutorial(false);
    }

    private void requestScreenshot() {
        mBus.post(new PugEvent.ActionScreenshotEvent());
    }

    public void showShareOverlay() {
        int translationY = (mRoot.getHeight() + mShareTextView.getHeight()) * -1;

        mShareOverlayView.setAlpha(0f);
        mShareOverlayView.setVisibility(View.VISIBLE);
        mShareTextView.setTranslationY(translationY);
        mShareTextView.setVisibility(View.VISIBLE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(
                ObjectAnimator.ofFloat(mShareOverlayView, View.ALPHA, 1f),
                ObjectAnimator.ofFloat(mShareTextView, View.TRANSLATION_Y, 0f));
        animatorSet.start();
    }

    public void hideShareOverlay(AnimatorListener animatorListener) {
        int translateY = (mRoot.getHeight() + mShareTextView.getHeight()) * -1;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(
                ObjectAnimator.ofFloat(mShareTextView, View.TRANSLATION_Y, translateY),
                ObjectAnimator.ofFloat(mShareOverlayView, View.ALPHA, 0f));
        animatorSet.addListener(new SimpleAnimatorListener(){
            @Override
            public void onAnimationEnd(Animator animation) {
                mShareOverlayView.setVisibility(View.GONE);
                mShareTextView.setVisibility(View.GONE);
            }
        });

        if (animatorListener != null) {
            animatorSet.addListener(animatorListener);
        }

        animatorSet.start();
    }

    public void showRatingTutorial() {
        if (!mTutorialPreferences.hasShown(Tutorial.RATING)) {
            mRatingTutorialView.setAlpha(0f);
            mRatingTutorialView.setVisibility(View.VISIBLE);
            ObjectAnimator.ofFloat(mRatingTutorialView, View.ALPHA, 0f, 1f).start();
        }
    }

    public void hideRatingTutorial(boolean updatePreferences) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mRatingTutorialView, View.ALPHA, 0f);
        animator.addListener(new SimpleAnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRatingTutorialView.setVisibility(View.GONE);
            }
        });
        animator.start();

        if (updatePreferences && !mTutorialPreferences.hasShown(Tutorial.RATING)) {
            mTutorialPreferences.setShown(Tutorial.RATING, true);
        }
    }

    public void showSharingTutorial() {
        if (!mTutorialPreferences.hasShown(Tutorial.SHARE)) {
            mShaingTutorialView.setAlpha(0f);
            mShaingTutorialView.setVisibility(View.VISIBLE);
            ObjectAnimator.ofFloat(mShaingTutorialView, View.ALPHA, 0f, 1f).start();
        }
    }

    public void hideSharingTutorial(boolean updatePreferences) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mShaingTutorialView, View.ALPHA, 0f);
        animator.addListener(new SimpleAnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mShaingTutorialView.setVisibility(View.GONE);
            }
        });
        animator.start();

        if (updatePreferences && !mTutorialPreferences.hasShown(Tutorial.SHARE)) {
            mTutorialPreferences.setShown(Tutorial.SHARE, true);
        }
    }

    public void cancelLoading() {
        Log.v(TAG, "[cancelLoading]");
        if (mRekoTask != null) {
            mRekoTask.cancel(true);
            mRekoTask = null;
        }

        mPugTextView.removeCallbacks(mPugTextLoadingRunnable);
    }

    private boolean isRekognitionEnabled() {
        return mRekognitionProperties != null && mRekoApi != null && mRekognitionProperties.isEnabled();
    }

    private void makeRekognitionRequest(Bitmap bitmap, OnTaskCompleteListener<Response> callback) {
        if (!isRekognitionEnabled()) {
            Log.w(TAG, "Rekognition api is not enabled or misconfigured! Simulating call...");
            callback.onComplete(Response.makeMockResponse(200, 0.021349f));
            return;
        }

        mRekoTask = new RekognitionTask(mRekoApi, callback).execute(bitmap);
    }

    private OnTaskCompleteListener<Response> mOnRekoTaskCompleteListener = new OnTaskCompleteListener<Response>() {
        @Override
        public void onComplete(Response response) {
            Log.i(TAG, "[onComplete] code=" + response.getStatusCode() + ", contents=" + response.getContents());
            cancelLoading();

            if (response.getStatusCode() == 200) {
                try {
                    JSONObject jsonObject = new JSONObject(response.getContents());
                    FaceDetectionResponse faceDetectionResponse = new RekoApi.FaceDetectionResponse(jsonObject);
                    List<Face> faces = faceDetectionResponse.getFaces();
                    if (faces.size() > 0) {
                        // NOTE: we only care about the first face
                        Face face = faces.get(0);
                        double score = face.getBeauty() * 1000d;
                        score = 1000d - score;
                        final PugRating pugRating = new PugExpression.PugRating((int) Math.floor(score));
                        Log.v(TAG, "[onComplete] rating=" + pugRating.getValue());
                        final PugExpression pugExpression = getSelectedPug().getExpression(pugRating);
                        setExpression(pugExpression);
                        setRating(pugRating);
                    } else {
                        Log.w(TAG, "[onComplete] No faces detected!");
                        setExpression(getSelectedPug().getErrorExtression());
                    }
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                    setExpression(getSelectedPug().getErrorExtression());
                }
            } else {
                Log.e(TAG, "[onComplete] Error from api: " + response.getStatusCode());
                setExpression(getSelectedPug().getErrorExtression());
            }
        }
    };

    private Runnable mPugTextLoadingRunnable = new Runnable() {

        private static final int CHAR_INTERVAL_MS = 80;
        private static final int LOADING_INTERVAL_MS = 2000;

        @Override
        public void run() {
            mPugTextView.animateText(new TypeWriterTextAnimator(getString(R.string.pug_mug_loading), CHAR_INTERVAL_MS));
            mPugTextView.postDelayed(this, LOADING_INTERVAL_MS);
        }
    };

    private AnimationCallbacks mRatingAnimationCallbacks = new AnimationCallbacks() {

        @Override
        public void onAnimationEnded(TextAnimator textAnimator) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(
                    ObjectAnimator.ofFloat(mRatingTextView, View.SCALE_X, 1f, 1.2f, 1f),
                    ObjectAnimator.ofFloat(mRatingTextView, View.SCALE_Y, 1f, 1.2f, 1f),
                    ObjectAnimator.ofFloat(mRatingTextView, View.ROTATION, 0f, 10f, -10f, 5f, -5f,  0f));
            animatorSet.addListener(new SimpleAnimatorListener() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    showRatingTutorial();
                }
            });
            animatorSet.start();
        }
    };

    private OnClickListener mShareTextOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            hideShareOverlay(new SimpleAnimatorListener(){
                @Override
                public void onAnimationEnd(Animator animation) {
                    requestScreenshot();
                };
            });
        }
    };

    private OnTouchListener mDismissSharingTutorialOnTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideSharingTutorial(true);
            return false;
        }
    };

    private OnTouchListener mDismissRatingTutorialOnTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideRatingTutorial(true);
            showSharingTutorial();
            return false;
        }
    };

    private OnTouchListener mRootOnTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.v(TAG, "[onTouch]");
            return mRootGestureDetector.onTouchEvent(event);
        }
    };

    private OnGestureListener mRootOnGestureListener = new OnGestureListener() {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {}

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {}

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.v(TAG, "[onFling] velocityX=" + velocityX + ", velocityY=" + velocityY);
            if (velocityY > 500) {
                hideSharingTutorial(true);
                showShareOverlay();
            } else if (velocityY < 500) {
                hideShareOverlay(null);
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true; // required to callback onFling
        }
    };

    public static interface OnTaskCompleteListener<T> {
        public void onComplete(T result);
    }
}