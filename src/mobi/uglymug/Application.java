package mobi.uglymug;

import com.halfbit.tinybus.Bus;
import com.halfbit.tinybus.BusDepot;
import com.halfbit.tinybus.TinyBus;

public class Application extends android.app.Application implements BusDepot {

    private final TinyBus mBus = new TinyBus();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public Bus getBus() {
        return mBus;
    }
}
