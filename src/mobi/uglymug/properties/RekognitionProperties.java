package mobi.uglymug.properties;

import java.io.IOException;
import java.util.Properties;

public class RekognitionProperties {

    private Properties mProperties;

    public RekognitionProperties(PropertiesFileLoader loader) throws IOException {
        mProperties = loader.loadFile(RekognitionProperties.class.getClassLoader(), "rekognition.properties");
    }

    public boolean isEnabled() {
        return Boolean.parseBoolean(mProperties.getProperty("api.enabled", "false"));
    }

    public String getApiKey() {
        return mProperties.getProperty("api.key", "");
    }

    public String getApiSecret() {
        return mProperties.getProperty("api.secret", "");
    }
}
