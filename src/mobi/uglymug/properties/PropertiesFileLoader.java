package mobi.uglymug.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileLoader {

    /**
     *
     * @param classLoader The {@link ClassLoader} to search for the file
     * @param fileName Should be in a source directory for the given class loader
     * @return
     * @throws IOException
     */
    public Properties loadFile(ClassLoader classLoader, String fileName) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = classLoader.getResourceAsStream(fileName);
            if (inputStream == null) {
                throw new FileNotFoundException();
            }

            final Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {}
            }
        }
    }
}