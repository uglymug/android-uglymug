package mobi.uglymug.api.rekognition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;

public class RekoApi {
    private String mApiKey;
    private String mApiSecret;

    public RekoApi(String apiKey, String apiSecret) {
        mApiKey = apiKey;
        mApiSecret = apiSecret;
    }

    public Request newRequest() {
        Request request = new Request();
        request.add(new BasicNameValuePair("api_key", mApiKey));
        request.add(new BasicNameValuePair("api_secret", mApiSecret));
        return request;
    }

    public Response execute(Request request){
        Response response = new Response(0, null);
        HttpClient httpclient = new DefaultHttpClient();
        // Please refer http://www.rekognition.com/docs/ for more documentation
        HttpPost httppost = new HttpPost("http://rekognition.com/func/api/index.php");
        try {
            // parameters of HTTP request
            httppost.setEntity(new UrlEncodedFormEntity(request));
            // Make API call
            HttpResponse httpResponse = httpclient.execute(httppost);
            HttpEntity responseEntity = httpResponse.getEntity();
            if(responseEntity!=null) {
                //You can refer to the documentation pages on ReKogntion.com to understand the structure of response
                response = new Response(httpResponse.getStatusLine().getStatusCode(), EntityUtils.toString(responseEntity));
            }
        } catch (ClientProtocolException e) {
            Log.w("ClientProtocolException", e.getMessage());
        } catch (IOException e) {
            Log.w("IOException", e.getMessage());
        }

        return response;
    }

    public static class Request extends ArrayList<NameValuePair> {
        private static final long serialVersionUID = 660050991443385154L;
        private static final String TAG = Request.class.getSimpleName();

        private Request() {
        }

        public void addImage(String url) {
            if (url != null) {
                add(new BasicNameValuePair("urls", url));
            }
        }

        public void addImage(byte[] data) {
            if (data != null ) {
                final String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
                add(new BasicNameValuePair("base64", encodedImage));
            } else {
                Log.w(TAG, "Image data was null!");
            }
        }
    }

    public static class Response {
        private int mStatusCode;
        private String mContents;

        public Response(int statusCode, String contents) {
            mStatusCode = statusCode;
            mContents = contents;
        }

        public int getStatusCode() {
            return mStatusCode;
        }

        public String getContents() {
            return mContents;
        }

        public static Response makeMockResponse(int statusCode, float score) {
            String jsonString = "{\"url\": \"base64\", \"face_detection\": [{"
                + "\"boundingbox\": {\"tl\": {\"x\": 82.49,\"y\": 256},"
                + "\"size\": {\"width\": 628.62, \"height\": 628.62}}, "
                + "\"confidence\": 0.99,\"pose\": {\"roll\": -5.01,\"yaw\": -6.67, \"pitch\": 9.15},"
                + "\"beauty\": " + score + "}], \"ori_img_size\": {\"width\": 768, \"height\": 1280},"
                + "\"usage\": {\"quota\": 4972, \"status\": \"Succeed.\", \"api_id\": \"kavBnKGQp9L14mcw\"}}";

            return new Response(statusCode, jsonString);
        }
    }

    public static class FaceDetectionResponse {
        private static final String TAG = FaceDetectionResponse.class.getSimpleName();

        private List<Face> mFaces = new ArrayList<Face>();
        public FaceDetectionResponse(JSONObject responseJson) throws JSONException {
            JSONArray jsonFaces = responseJson.getJSONArray("face_detection");
            for (int i=0; i < jsonFaces.length(); i++) {
                try {
                    JSONObject jsonFace = jsonFaces.getJSONObject(i);
                    mFaces.add(new Face(jsonFace.getDouble("beauty")));
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }
            }
        }

        public List<Face> getFaces() {
            return Collections.unmodifiableList(mFaces);
        }
    }

    public static class Face {
        private double mBeauty;

        public Face(double beauty) {
            mBeauty = beauty;
        }

        public double getBeauty() {
            return mBeauty;
        }
    }
}
