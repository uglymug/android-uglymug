package mobi.uglymug.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Build;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

public class Cameras {

    private static final String MANUFACTURER_GENYMOTION = "Genymotion";

    public static enum CameraType {
        FRONT(PackageManager.FEATURE_CAMERA_FRONT),
        REAR(PackageManager.FEATURE_CAMERA);

        private String mKey;

        private CameraType(String key) {
            mKey = key;
        }

        public String getKey() {
            return mKey;
        }
    }

    public static boolean hasCamera(Context context, CameraType type) {
        if (context.getPackageManager().hasSystemFeature(type.getKey())){
            return true;
        } else {
            return false;
        }
    }

    public static int getCameraIdForType(CameraType cameraType) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int i = 0; i < cameraCount; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            switch (cameraType) {
                case FRONT:
                    if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                        return i;
                    }
                    break;
                case REAR:
                    if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                        return i;
                    }
                    break;
            }
        }

        throw new UnsupportedOperationException("Could not find camera!");
    }

    /**
     * Starting Froyo the camera preview comes rotated so we need to adjust the rotation to display it properly.
     * @param display {@link WindowManager#getDefaultDisplay()}
     * @return
     */
    public static int getRotationAngleForPreview(Display display) {
        if (Build.MANUFACTURER.contains(MANUFACTURER_GENYMOTION)) {
            return 0;
        }

        switch (display.getRotation()) {
            case Surface.ROTATION_0: // This is display orientation
                return  90; // This is camera orientation
            default:
            case Surface.ROTATION_90:
                return 0;
            case Surface.ROTATION_180:
                return 270;
            case Surface.ROTATION_270:
                return 180;
        }
    }

    /**
     * Starting Froyo the picture data comes rotated so we need to adjust the rotation to display it properly.
     * @param display {@link WindowManager#getDefaultDisplay()}
     * @return
     */
    public static int getRotationAngleForPicture(Display display) {
        if (Build.MANUFACTURER.contains(MANUFACTURER_GENYMOTION)) {
            return 0;
        }

        switch (display.getRotation()) {
            default:
            case Surface.ROTATION_0: // This is display orientation
                return 270; // This is camera orientation
            case Surface.ROTATION_90:
                return 0;
            case Surface.ROTATION_180:
                return 90;
            case Surface.ROTATION_270:
                return 180;
        }
    }
}
