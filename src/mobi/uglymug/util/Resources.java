package mobi.uglymug.util;

import android.content.Context;
import android.content.res.TypedArray;

public class Resources {

    public static String[] getStrings(Context context, int arrayRes) {
        return context.getResources().getStringArray(arrayRes);
    }

    public static int[] getResouceIds(Context context, int arrayRes) {
        TypedArray resources = context.getResources().obtainTypedArray(arrayRes);
        int[] ids = new int[resources.length()];
        for (int i = 0; i < ids.length; i++) {
            ids[i] = resources.getResourceId(i, 0);
        }
        resources.recycle();
        return ids;
    }
}
