package mobi.uglymug.util;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

public class Screenshots {
    public static void takeScreenshot(final View rootView, final OnScreenshotTaken callback) {
        rootView.setDrawingCacheEnabled(true);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootView.buildDrawingCache(true);
                Bitmap drawingCache = rootView.getDrawingCache();
                callback.onScreenshotTaken(drawingCache);
                rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        rootView.requestLayout();
    }

    public static interface OnScreenshotTaken {
        public void onScreenshotTaken(Bitmap bitmap);
    }
}
