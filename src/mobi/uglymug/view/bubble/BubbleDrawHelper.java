package mobi.uglymug.view.bubble;

import mobi.uglymug.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Join;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class BubbleDrawHelper implements Bubble {
    private int mCornerRadius;

    private int mStrokeWidth;
    private int mStrokeColor = 0x00FFFFFF;
    private final Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final RectF mStrokeRect = new RectF();

    private int mBackgroundColor = 0x00FFFFFF;
    private final Paint mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final RectF mBackgroundRect = new RectF();

    private boolean mShowDropShadow;
    private int mDropShadowColor = 0x000000;
    private int mDropShadowOffsetX;
    private int mDropShadowOffsetY;
    private final Paint mDropShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final RectF mDropShadowRect = new RectF();

    public BubbleDrawHelper() {
    }

    private void configurePaint() {
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        mBackgroundPaint.setColor(mBackgroundColor);

        mDropShadowPaint.setStyle(Paint.Style.STROKE);
        mDropShadowPaint.setColor(mDropShadowColor);
        mDropShadowPaint.setStrokeWidth(mStrokeWidth);
        mDropShadowPaint.setStrokeJoin(Join.ROUND);

        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStrokeWidth(mStrokeWidth);
        mStrokePaint.setStrokeJoin(Join.ROUND);
    }

    public void readAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BubbleDrawHelper);
        mCornerRadius = a.getInt(R.styleable.BubbleDrawHelper_cornerRadius, mCornerRadius);
        mStrokeWidth = a.getInt(R.styleable.BubbleDrawHelper_strokeWidth, mStrokeWidth);
        mStrokeColor = a.getColor(R.styleable.BubbleDrawHelper_strokeColor, mStrokeColor);
        mBackgroundColor = a.getColor(R.styleable.BubbleDrawHelper_backgroundColor, mBackgroundColor);
        mShowDropShadow = a.getBoolean(R.styleable.BubbleDrawHelper_showDropShadow, mShowDropShadow);
        mDropShadowOffsetX = a.getInt(R.styleable.BubbleDrawHelper_dropShadowOffsetX, mDropShadowOffsetX);
        mDropShadowOffsetY = a.getInt(R.styleable.BubbleDrawHelper_dropShadowOffsetY, mDropShadowOffsetY);
        mDropShadowColor = a.getColor(R.styleable.BubbleDrawHelper_dropShadowColor, mDropShadowColor);
        a.recycle();

        configurePaint();
    }

    /**
     * Since this method draws the background on the given canvas, it should be called BEFORE {@link View#onDraw}
     * @param canvas
     */
    public void onDraw(Canvas canvas) {
        if (mShowDropShadow) {
            canvas.drawRoundRect(mDropShadowRect, mCornerRadius, mCornerRadius, mDropShadowPaint);
        }
        canvas.drawRoundRect(mBackgroundRect, mCornerRadius, mCornerRadius, mBackgroundPaint);
        canvas.drawRoundRect(mStrokeRect, mCornerRadius, mCornerRadius, mStrokePaint);
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        mBackgroundRect.set(mStrokeWidth, mStrokeWidth, w - mStrokeWidth, h - mStrokeWidth);
        mDropShadowRect.set(mStrokeWidth + mDropShadowOffsetX, mStrokeWidth + mDropShadowOffsetY,
                w - mStrokeWidth + mDropShadowOffsetX, h - mStrokeWidth + mDropShadowOffsetY);
        mStrokeRect.set(mStrokeWidth, mStrokeWidth, w - mStrokeWidth, h - mStrokeWidth);
    }

    @Override
    public void setCornerRadius(int radius) {
        mCornerRadius = radius;
    }

    @Override
    public void setStrokeWidth(int width) {
        mStrokeWidth = width;
        configurePaint();
    }

    @Override
    public void setStrokeColor(int color) {
        mStrokeColor = color;
        configurePaint();
    }

    @Override
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
        configurePaint();
    }

    @Override
    public void setShowDropShadow(boolean showDropShadow) {
        mShowDropShadow = showDropShadow;
    }

    @Override
    public void setDropShadowColor(int color) {
        mDropShadowColor = color;
        configurePaint();
    }

    @Override
    public void setDropShadowOffset(int x, int y) {
        mDropShadowOffsetX = x;
        mDropShadowOffsetY = y;
    }
}