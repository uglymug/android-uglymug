package mobi.uglymug.view.bubble;

import mobi.uglymug.view.text.FontHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class BubbleTextView extends TextView implements Bubble {

    private BubbleDrawHelper mBubbleDrawHelper;
    private FontHelper mFontHelper;

    public BubbleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public BubbleTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BubbleTextView(Context context) {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        mBubbleDrawHelper = new BubbleDrawHelper();
        mBubbleDrawHelper.readAttrs(context, attrs);
        mFontHelper = new FontHelper(context);
        mFontHelper.bindAttributes(this, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBubbleDrawHelper.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    @SuppressLint("WrongCall")
    public void onDraw(Canvas canvas) {
        mBubbleDrawHelper.onDraw(canvas);
        super.onDraw(canvas);
    }

    /***************************
     * Bubble interface
     */

    @Override
    public void setCornerRadius(int radius) {
        mBubbleDrawHelper.setCornerRadius(radius);
    }

    @Override
    public void setStrokeWidth(int width) {
        mBubbleDrawHelper.setStrokeWidth(width);
    }

    @Override
    public void setStrokeColor(int color) {
        mBubbleDrawHelper.setStrokeColor(color);
    }

    @Override
    public void setBackgroundColor(int color) {
        mBubbleDrawHelper.setBackgroundColor(color);
    }

    @Override
    public void setShowDropShadow(boolean dropShadow) {
        mBubbleDrawHelper.setShowDropShadow(dropShadow);
    }

    @Override
    public void setDropShadowColor(int color) {
        mBubbleDrawHelper.setDropShadowColor(color);
    }

    @Override
    public void setDropShadowOffset(int x, int y) {
        mBubbleDrawHelper.setDropShadowOffset(x, y);
    }
}