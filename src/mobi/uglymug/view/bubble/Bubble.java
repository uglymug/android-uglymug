package mobi.uglymug.view.bubble;


public interface Bubble {

    public void setCornerRadius(int radius);

    public void setStrokeWidth(int width);

    public void setStrokeColor(int color);

    public void setBackgroundColor(int color);

    public void setShowDropShadow(boolean dropShadow);

    public void setDropShadowColor(int color);

    public void setDropShadowOffset(int x, int y);
}
