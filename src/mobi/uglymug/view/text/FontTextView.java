package mobi.uglymug.view.text;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {

    private FontHelper mFontHelper;

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontTextView(Context context) {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        mFontHelper = new FontHelper(context);
        mFontHelper.bindAttributes(this, attrs);
    }
}
