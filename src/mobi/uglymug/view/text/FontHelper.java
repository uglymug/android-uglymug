package mobi.uglymug.view.text;

import mobi.uglymug.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontHelper {
    private Context mContext;

    public FontHelper(Context context) {
        mContext = context;
    }

    public void bindAttributes(TextView textView, AttributeSet attrs) {
        if (attrs != null && !textView.isInEditMode()) {
            TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FontHelper);
            String fontFile = a.getString(R.styleable.FontHelper_fontFile);
            if (fontFile != null) {
                Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), fontFile);
                textView.setTypeface(myTypeface);
            }
            a.recycle();
        }
    }
}
