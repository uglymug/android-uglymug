package mobi.uglymug.view.text.animation;


public class TypeWriterTextAnimator implements TextAnimator {

    private int mCharacterDelayMs;
    private int mCurrentCharacter;
    private CharSequence mText;

    public TypeWriterTextAnimator(CharSequence text, int characterDelayMs) {
        mText = text;
        mCharacterDelayMs = characterDelayMs;
    }

    @Override
    public long getDelayMs() {
        return mCharacterDelayMs;
    }

    @Override
    public CharSequence getCurrentText() {
        CharSequence subSequence = mText.subSequence(0, mCurrentCharacter);
        mCurrentCharacter++;
        return subSequence;
    }

    @Override
    public boolean isDone() {
        return mText == null || mCurrentCharacter > mText.length();
    }

    public static int getTotalAnimationTime(String text, int charDelayMs) {
        if (text == null) {
            return 0;
        }

        return text.length() * charDelayMs;
    }
}