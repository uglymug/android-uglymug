package mobi.uglymug.view.text.animation;


public class CounterTextAnimator implements TextAnimator {

    private int mCurrentValue;
    private int mStartValue;
    private int mEndValue;
    private long mCharDelayMs;
    private AnimationCallbacks mAnimationCallbacks;
    private int mSteps;

    public CounterTextAnimator(int startValue, int endValue, long charDelayMs, int steps, AnimationCallbacks animationCallbacks) {
        mStartValue = mCurrentValue = startValue;
        mEndValue = endValue;
        mCharDelayMs = charDelayMs;
        mSteps = steps;
        mAnimationCallbacks = animationCallbacks;
    }

    @Override
    public long getDelayMs() {
        return mCharDelayMs;
    }

    @Override
    public CharSequence getCurrentText() {
        return "" + nextValue();
    }

    private int nextValue() {
       if (isDone()) {
           throw new IllegalStateException("nextValue() called after animation has completed.");
       }

       if (mStartValue < mEndValue) {
           mCurrentValue = mCurrentValue + mSteps;
           if (mCurrentValue > mEndValue) {
               mCurrentValue = mEndValue;
           }
       } else {
           mCurrentValue = mCurrentValue - mSteps;
           if (mCurrentValue < mEndValue) {
               mCurrentValue = mEndValue;
           }
       }

       return mCurrentValue;
    }

    @Override
    public boolean isDone() {

        final boolean isDone = (mCurrentValue == mEndValue);

        if (isDone && mAnimationCallbacks != null) {
            mAnimationCallbacks.onAnimationEnded(this);
            mAnimationCallbacks = null;
        }

        return isDone;
    }

    public static interface AnimationCallbacks {
        void onAnimationEnded(TextAnimator textAnimator);
    }
}
