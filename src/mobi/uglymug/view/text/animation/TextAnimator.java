package mobi.uglymug.view.text.animation;

public interface TextAnimator {
    public long getDelayMs();
    public CharSequence getCurrentText();
    public boolean isDone();
}
