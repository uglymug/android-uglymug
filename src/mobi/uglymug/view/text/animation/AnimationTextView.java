package mobi.uglymug.view.text.animation;

import mobi.uglymug.view.bubble.Bubble;
import mobi.uglymug.view.bubble.BubbleDrawHelper;
import mobi.uglymug.view.text.FontHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.TextView;

public class AnimationTextView extends TextView implements Bubble {

    private BubbleDrawHelper mBubbleDrawHelper;
    private FontHelper mFontHelper;
    private TextAnimator mTextAnimator;

    public AnimationTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public AnimationTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimationTextView(Context context) {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        mBubbleDrawHelper = new BubbleDrawHelper();
        mBubbleDrawHelper.readAttrs(context, attrs);
        mFontHelper = new FontHelper(context);
        mFontHelper.bindAttributes(this, attrs);
    }

    public void animateText(TextAnimator textAnimator) {
        mTextAnimator = textAnimator;
        postInvalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBubbleDrawHelper.onSizeChanged(w, h, oldw, oldh);
    }

    private long mLastTime;

    @Override
    @SuppressLint("WrongCall")
    public void onDraw(Canvas canvas) {
        mBubbleDrawHelper.onDraw(canvas);

        if (!isInEditMode()) {
            final long time = SystemClock.uptimeMillis();
            final long delta = time - mLastTime;
            if (mTextAnimator != null && !mTextAnimator.isDone() && delta > mTextAnimator.getDelayMs()) {
                setText(mTextAnimator.getCurrentText(), TextView.BufferType.SPANNABLE);
                postInvalidateDelayed(mTextAnimator.getDelayMs());
                mLastTime = time;
            }
        }

        super.onDraw(canvas);
    }

    /***************************
     * Bubble interface
     */

    @Override
    public void setCornerRadius(int radius) {
        mBubbleDrawHelper.setCornerRadius(radius);
    }

    @Override
    public void setStrokeWidth(int width) {
        mBubbleDrawHelper.setStrokeWidth(width);
    }

    @Override
    public void setStrokeColor(int color) {
        mBubbleDrawHelper.setStrokeColor(color);
    }

    @Override
    public void setBackgroundColor(int color) {
        mBubbleDrawHelper.setBackgroundColor(color);
    }

    @Override
    public void setShowDropShadow(boolean dropShadow) {
        mBubbleDrawHelper.setShowDropShadow(dropShadow);
    }

    @Override
    public void setDropShadowColor(int color) {
        mBubbleDrawHelper.setDropShadowColor(color);
    }

    @Override
    public void setDropShadowOffset(int x, int y) {
        mBubbleDrawHelper.setDropShadowOffset(x, y);
    }
}
