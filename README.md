# UglyMug #

Do you know how ugly you are? Do you know how ugly you can be? Well don't just ask your friends. Have a concrete scientific\* rating today from *Mug*, the ugly rating pug! Simply grab the latest **UglyMug** app from the Play Store (link coming soon).

** References needed*

## Developers ##

Want to enhance the experience or fix some of those pesky bugs? Then, feel free to submit a pull request. We also wouldn't take it personal if you stole our code. :)

### Rekognition API setup ###

* Get an API key from Rekognition: https://rekognition.com/
* Create a **rekognition.properties** file in your ***src*** directory and enter the following:

```
#!properties

api.enabled=true
api.key=<API_KEY>
api.secret=<API_SECRET>
```

## License ##

```
#!text

                      NO NONSENSE BEERWARE LICENSE
                              Version 1
                  
You can do whatever you want with this stuff. If we meet some day, and 
you think this stuff is worth it, you can buy me/us a beer in return.
```